import { TransferTransaction, Deadline, PublicAccount, NetworkType, 
        PlainMessage, NetworkCurrencyMosaic} from 'tsjs-xpx-chain-sdk';

import { TransactionRequestMessage } from '../src/message/TransactionRequestMessage';

/*Data get from user input*/
const publicKey:string = undefined;
const number:number = undefined;
const message:string = undefined;

const recipient = PublicAccount.createFromPublicKey(
    publicKey,
    NetworkType.MAIN_NET);

const mosaic = NetworkCurrencyMosaic.createRelative(number);

const tx = TransferTransaction.create(
    Deadline.create(),
    recipient.address,
    [mosaic],
    PlainMessage.create(message),
    NetworkType.MAIN_NET
);

let TxMessage = new TransactionRequestMessage('',tx,'');
let qr = TxMessage.generateQR();

