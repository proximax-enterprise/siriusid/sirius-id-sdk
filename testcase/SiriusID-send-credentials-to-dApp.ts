import { CredentialRequestMessage } from '../src/message/CredentialRequestMessage';
import { MessageType } from '../src/message/MessageType';
import { Credentials } from '../src/message/Credentials';
import { MessageReceived } from '../src/service/MessageReceived';

let message = undefined; //receive from QRCode or deeplink
 
let messageReceived = new MessageReceived(message);
let id = messageReceived.message.payload.credential.id;

/*Use 'id' to search in stored to get credential*/
const credential:Credentials = undefined;

const credentialMessage = new CredentialRequestMessage(MessageType.CREDENTIAL_RES,credential);

const qr = credentialMessage.generateQR();

