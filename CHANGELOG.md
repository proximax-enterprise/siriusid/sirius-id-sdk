# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.58](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.57...v0.0.58) (2020-09-22)

### [0.0.57](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.56...v0.0.57) (2020-09-16)

### [0.0.56](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.55...v0.0.56) (2020-09-08)

### [0.0.55](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.54...v0.0.55) (2020-09-04)

### [0.0.54](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.53...v0.0.54) (2020-09-04)

### [0.0.53](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.52...v0.0.53) (2020-09-04)

### [0.0.52](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.51...v0.0.52) (2020-09-04)

### [0.0.51](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.50...v0.0.51) (2020-09-04)

### [0.0.50](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.49...v0.0.50) (2020-09-04)

### [0.0.49](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.48...v0.0.49) (2020-09-04)

### [0.0.48](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.47...v0.0.48) (2020-09-04)

### [0.0.47](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.46...v0.0.47) (2020-09-03)

### [0.0.46](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.45...v0.0.46) (2020-09-01)

### [0.0.45](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.44...v0.0.45) (2020-09-01)

### [0.0.44](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.43...v0.0.44) (2020-09-01)

### [0.0.43](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.42...v0.0.43) (2020-09-01)

### [0.0.42](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.41...v0.0.42) (2020-07-27)

### [0.0.41](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.40...v0.0.41) (2020-07-01)

### [0.0.40](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.39...v0.0.40) (2020-07-01)

### [0.0.39](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.38...v0.0.39) (2020-06-24)

### [0.0.38](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.37...v0.0.38) (2020-06-03)

### [0.0.37](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.36...v0.0.37) (2020-06-03)

### [0.0.36](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.35...v0.0.36) (2020-06-03)

### [0.0.35](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.34...v0.0.35) (2020-06-02)

### [0.0.34](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.33...v0.0.34) (2020-05-25)

### [0.0.33](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.32...v0.0.33) (2020-05-21)

### [0.0.32](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.31...v0.0.32) (2020-05-19)

### [0.0.31](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.30...v0.0.31) (2020-05-06)

### [0.0.30](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.29...v0.0.30) (2020-05-05)

### [0.0.29](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.28...v0.0.29) (2020-05-05)

### [0.0.28](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.27...v0.0.28) (2020-05-05)

### [0.0.27](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.26...v0.0.27) (2020-04-29)

### [0.0.26](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.25...v0.0.26) (2020-04-28)

### [0.0.25](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.24...v0.0.25) (2020-04-28)

### [0.0.24](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.23...v0.0.24) (2020-04-24)

### [0.0.23](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.22...v0.0.23) (2020-04-02)

### [0.0.22](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.21...v0.0.22) (2020-04-02)

### [0.0.21](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.20...v0.0.21) (2020-04-02)

### [0.0.20](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.19...v0.0.20) (2020-04-02)

### [0.0.19](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.18...v0.0.19) (2020-04-01)

### [0.0.18](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.17...v0.0.18) (2020-04-01)

### [0.0.17](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.16...v0.0.17) (2020-04-01)

### [0.0.16](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.15...v0.0.16) (2020-04-01)

### [0.0.15](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.14...v0.0.15) (2020-04-01)

### [0.0.14](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.13...v0.0.14) (2020-04-01)

### [0.0.12](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.13...v0.0.12) (2020-04-01)

### [0.0.13](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.12...v0.0.13) (2020-04-01)

### [0.0.12](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.11...v0.0.12) (2020-04-01)

### [0.0.11](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.10...v0.0.11) (2020-03-28)

### [0.0.10](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.9...v0.0.10) (2020-03-27)

### [0.0.9](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.8...v0.0.9) (2020-03-27)

### [0.0.8](https://bitbucket.org/fdssoft_com/siriusid_sdk/compare/v0.0.7...v0.0.8) (2020-03-27)

### 0.0.7 (2020-03-23)
