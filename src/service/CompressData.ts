const zlib = require('zlib');
export class CompressData{
    constructor(){}

    static async compress(data:string):Promise<string>{
        return new Promise((resolve,reject) => {
            zlib.deflate(data, (err:any, buffer:any) => {
                if (!err) {
                  resolve(buffer.toString('base64'));
                } else {
                  console.log(err);
                }
            });
        }); 
    }

    static async decompress(data:string):Promise<string>{
        const buffer = Buffer.from(data, 'base64');
        return new Promise((resolve,reject) => {
            zlib.unzip(buffer, (err:any, buffer:any) => {
                if (!err) {
                  resolve(buffer.toString());
                } else {
                  console.log(err);
                }
              });
        });
        
    }
}