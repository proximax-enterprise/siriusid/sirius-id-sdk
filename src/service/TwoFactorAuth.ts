var twoFactor = require('node-2fa');

export class TwoFactorAuth{
    static secret:any;

    constructor(){}

    static createSecret(name:string,account:string){
        this.secret = twoFactor.generateSecret({name: name, account: account});
    }
    
    static generateQRFromSecret(secret:string,name:string,account:string){
        return "https://chart.googleapis.com/chart?chs=166x166&chld=L|0&cht=qr&chl=otpauth://totp/" + name + "%3A" + account + "%3F=" + secret;
    }

    static generateURIFromSecret(secret:string,name:string,account:string){
        return "otpauth://totp/" + name + "%3A" + account + "%3Fsecret=" + secret;
    }

    static verifyToken(secret:string,token:string){
        return twoFactor.verifyToken(secret, token);
    }

    static generateToken(secret:String){
        return twoFactor.generateToken(secret);
    }
}