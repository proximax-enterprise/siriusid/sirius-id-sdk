import { NetworkType } from "tsjs-xpx-chain-sdk";

export class ApiNode{
    static apiNode: string;
    static networkType: NetworkType;
}