import {
    TransferTransaction,
    EncryptedMessage,
    PublicAccount
} from 'tsjs-xpx-chain-sdk';

import {CredentialStored} from '../message/CredentialStored';
import { AdditionStored } from '../message/AdditionStored';
import { CredentialRequired } from '../message';
export class VerifytoLogin{
    // Compare between sessionToken generate by dApp and encryptedSessionToken sent from SiriusID to verify
    success = false;
    static message: any;
    static credentials: any[];
    static addition:any[];

    static async verify(transactionReceived: any, sessionToken: string, credentials:CredentialRequired[],privateKey:string, addition ? :any){
        const transferTx = transactionReceived as TransferTransaction;
        const senderPublicKey = transferTx.signer?.publicKey;
        const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
        const plainMessage = EncryptedMessage.decrypt(transferTxMessage,privateKey,<PublicAccount>transferTx.signer);
        
        const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
        VerifytoLogin.message = JSONtransferTxmessage;
        let step1 = await this.verifyCredentials(sessionToken,JSONtransferTxmessage,senderPublicKey,credentials);
        let step2 = false;
        if (addition){
            step2 = await this.verifyAddition(JSONtransferTxmessage,addition);
        }
        else step2 = true;
        
        if(step1 && step2){
            return true;
        }
        else return false;      
    }

    constructor (
        transaction:any,
        sessionToken: string,
        credentials:string[],
        privateKey: string
    ){}

    // static intersecArray(arr1:any[], arr2:any[]){
    //     return arr1.filter(element1 => arr2.includes(element1['id']));
    // }

    /**
     * Get array of intersection elements from 2 arrays
     * @param arr1 received array
     * @param arr2 required array
     */
    static findMapArray(arr1: any[], arr2:any[]){
        let resultArray = new Array();
        for (let i=0;i<arr2.length;i++){
            let idRequired = arr2[i].id;
            for (let j=0;j<arr1.length;j++){
                if (arr1[j]['id'] == idRequired){
                    resultArray.push(arr1[j]);
                }
            }
        }
        return resultArray;
    }

    static getMessage(){
        return VerifytoLogin.message;
    }

    static async verifyCredentials(sessionToken:string, JSONtransferTxmessage:any,senderPublicKey:string | undefined,credentials:CredentialRequired[]){
        let credentialsReceived = new Array();
        for (let i=0;i<JSONtransferTxmessage.payload.credentials.length;i++){
            let el = new CredentialStored(JSONtransferTxmessage.payload.credentials[i]['keyDecrypt'],JSONtransferTxmessage.payload.credentials[i]['credentialHash']);
            let credential = await el.getCredential(el.credentialHash,el.keyDecrypt,<string>senderPublicKey);
            credentialsReceived.push(credential);
        }
        let credentials_req = this.findMapArray(credentialsReceived,credentials);
        //Check if message received is a LOGIN message
        if(JSONtransferTxmessage.messageType == 1){
            if (sessionToken == JSONtransferTxmessage.payload.encryptedSessionToken
                && credentials_req.length == credentials.length
            ){
                VerifytoLogin.credentials = credentialsReceived;
                console.log("Login successfully");
                return true;
            }
            else{
                console.log("Login failed");
                return false;
            }
        }
    }

    static async verifyAddition(JSONtransferTxmessage:any,addition:any){
        let additionStored = new AdditionStored(JSONtransferTxmessage.payload.addition['publicKey'],JSONtransferTxmessage.payload.addition['privateKey'],JSONtransferTxmessage.payload.addition['additionHash']);
        let additionReceived = await additionStored.getAddition();

        if(addition.length != additionReceived.length){
            return false;
        }
        
        for (let i=0;i<addition.length;i++){
            if (addition[i]['name'] != additionReceived[i]['name']){
                return false;
            }
        }
        VerifytoLogin.addition = additionReceived;
        return true;
    }
}