import { MessageType } from '../message/MessageType';
import { LoginRequestMessage } from '../message/LoginRequestMessage';
import { TransactionRequestMessage } from '../message/TransactionRequestMessage';
import { CredentialRequestMessage } from '../message/CredentialRequestMessage';
import { CredentialConfirmMessage } from '../message/CredentialConfirmMessage';
import { VerifyRequestMessage } from '../message/VerifyRequestMessage';
import { TwoFAMessage } from '../message';

export class MessageReceived{

    public message:any;

    constructor(messageReceived:string){
        let messageObj = JSON.parse(messageReceived);
        let result = null;
        switch (messageObj.type){
            case MessageType.LOG_IN:
                result = new LoginRequestMessage(
                    messageObj.payload.appPublicKey,
                    messageObj.payload.sessionToken,
                    messageObj.payload.credentials,
                    messageObj.payload.addition
                );
                break;
            case MessageType.TRANSACTION:
                if(messageObj.payload.transaction){
                    result = new TransactionRequestMessage(
                        messageObj.payload.message,
                        messageObj.payload.transaction.transaction,
                        messageObj.payload.transactionHash
                    );
                }
                else result = new TransactionRequestMessage(
                        messageObj.payload.message,
                        null,
                        messageObj.payload.transactionHash
                    );
                break;
            case MessageType.CREDENTIAL:
                result = new CredentialRequestMessage(messageObj.payload.credentials);
                break;
            case MessageType.CREDENTIAL_REQ:
                result = new CredentialConfirmMessage(
                    messageObj.payload.appPublicKey,
                    messageObj.payload.sessionToken,
                    messageObj.payload.credential
                );
                break;
            case MessageType.VERIFY_HARD:
                result = new VerifyRequestMessage(
                    messageObj.payload.appPublicKey,
                    messageObj.payload.sessionToken,
                    messageObj.payload.credentials,
                    MessageType.VERIFY_HARD
                );
                break;
            case MessageType.VERIFY_LIGHT:
                result = new VerifyRequestMessage(
                    messageObj.payload.appPublicKey,
                    messageObj.payload.sessionToken,
                    messageObj.payload.credentials,
                    MessageType.VERIFY_LIGHT,
                    messageObj.payload.url
                );
                break;
            case MessageType.VERIFY_BOTH:
                result = new VerifyRequestMessage(
                    messageObj.payload.appPublicKey,
                    messageObj.payload.sessionToken,
                    messageObj.payload.credentials,
                    MessageType.VERIFY_BOTH,
                    messageObj.payload.url
                );
                break;

            case MessageType.TWO_FA:
                result = new TwoFAMessage(
                    messageObj.payload.sessionToken,
                    messageObj.payload.name,
                    messageObj.payload.account,
                    messageObj.payload.secret
                );
                break;    

            // case MessageType.CREDENTIAL_RES:
            //     result = new CredentialRequestMessage(messageObj.type,messageObj.payload.credential);
            //     break;
        }
        this.message = result;
    }
}