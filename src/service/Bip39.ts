var bip39 = require('bip39');

export class Bip39{
    constructor(){}

    static EntropyToMnemonic(privateKey:string){
        return bip39.entropyToMnemonic(privateKey);
    }

    static MnemonicToEntropy(passPhrase:string){
        return bip39.mnemonicToEntropy(passPhrase);
    }
}