export class CredentialRequired{

    private constructor(
        public id:string,
        public option ? :string[],
    ){
    }

    static create(
        id:string,
        option ? :string[],
    ){
        return new CredentialRequired(id,option);
    }

    getId(){
        return this.id;
    }

    getOption(){
        return this.option;
    }

}