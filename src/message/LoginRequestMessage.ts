import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import { AdditionRequire } from './AdditionRequire';
import { CredentialRequired } from './CredentialRequired';

export class LoginRequestMessage extends AppMessage {

    /**
     * Creation Date of message
     */
    createTimestamp: number;

    private token:string;
    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     * 
     * @param credentials array id of credentials dApp need
     */
    public static create(publicKey: string,credentials:CredentialRequired[], addition ? : AdditionRequire[]) {
        const token = this.generateToken();
        return new LoginRequestMessage(publicKey, token, credentials, addition);
    }

    constructor(
        /**
         * dApp 
         */
        appPublicKey: string,
        /**
         * token
         */
        sessionToken: string,
        /**
         * list id
         */
        credentials:CredentialRequired[],
        /**
         * addition require
         */
        addition ? : AdditionRequire[]
    ) {
        const type = MessageType.LOG_IN;
        const payload = {
            appPublicKey,
            sessionToken,
            credentials,
            addition
        }
        super(type, payload);

        this.createTimestamp = Date.now();
        this.token = payload.sessionToken;
    }

    /**
     * Generate random string of sesion token
     */
    private static generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 64; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }

    getSessionToken(){
        return this.token;
    }
}