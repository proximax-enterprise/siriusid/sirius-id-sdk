import { Account, NetworkType, EncryptedMessage, PublicAccount } from "tsjs-xpx-chain-sdk";
import { Credentials } from "./Credentials";
import { IpfsConnection, BlockchainNetworkConnection, Uploader, Downloader, ConnectionConfig, BlockchainNetworkType, Protocol, StringParameterData, UploadParameter, DownloadParameter } from 'tsjs-chain-xipfs-sdk';
import {ApiNode} from '../service/ApiNode';
export class CredentialStored{

    static readonly NETWORK = NetworkType.MAIN_NET;
    static readonly ipfsDomain = "ipfs1-dev.xpxsirius.io";

    // static readonly ipfsPortUp = 5001;
    // static readonly ipfsProtocolUp = "http";

    // static readonly ipfsPortDown = 443;
    // static readonly ipfsProtocolDown = "https";

    static readonly ipfsPort = 5443;
    static readonly ipfsProtocol = "https";


    constructor(
        public keyDecrypt:string,
        public credentialHash: any,
    ){
    }

    static async create(
        credential: Credentials,
        privateKey: any
    ){
        const account = Account.generateNewAccount(CredentialStored.NETWORK);
        const keyEncrypt = account.publicKey;
        const keyDecrypt = account.privateKey;
        const credentialHash = await CredentialStored.storedCredential(credential,keyEncrypt,privateKey);
        
        return new CredentialStored(keyDecrypt,credentialHash);
    }

    static async storedCredential(credential: Credentials,keyEncrypt:string, privateKey:string){
        const strCredential = JSON.stringify(credential);
        const publicAccount = PublicAccount.createFromPublicKey(keyEncrypt,CredentialStored.NETWORK);
        const encStr = EncryptedMessage.create(strCredential,publicAccount,privateKey);
        const sender = Account.createFromPrivateKey(privateKey,ApiNode.networkType);
        const ipfsConnection = new IpfsConnection(
            CredentialStored.ipfsDomain, // the host or multi address
            CredentialStored.ipfsPort, // the port number
            { protocol: CredentialStored.ipfsProtocol } // the optional protocol
        );
    
        // Creates Proximax blockchain network connection
        const apiHost = this.convertApiNode();
        const blockchainConnection = new BlockchainNetworkConnection(
            CredentialStored.convertNetworkType(), // the network type
            apiHost.apiDomain, // the rest api base endpoint
            apiHost.apiPort, // the optional websocket end point
            apiHost.apiProtocol
        );
    
        // Connection Config
        const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);
    
        //
        const uploader = new Uploader(conectionConfig);
        // const dowloader = new Downloader(conectionConfig);

        const strParam = StringParameterData.create(encStr.payload);
        const uploadParamBuilder = UploadParameter.createForStringUpload(strParam,privateKey);
        const uploadParams = uploadParamBuilder
          .withRecipientPublicKey(sender.publicKey)
          .withTransactionMosaics([])
          .build();
        return new Promise(resolve => {
            uploader.upload(uploadParams).then(result => {
                console.log("transaction hash: " + result.transactionHash);
                resolve(result.transactionHash);
            });
        })
    }

    getKeyDecrypt(){
        return this.keyDecrypt;
    }

    getCredentialHash(){
        return this.credentialHash;
    }

    static convertNetworkType(){
        switch (ApiNode.networkType){
            case NetworkType.MAIN_NET: 
                return BlockchainNetworkType.MAIN_NET;
            case NetworkType.TEST_NET: 
                return BlockchainNetworkType.TEST_NET;
            case NetworkType.MIJIN: 
                return BlockchainNetworkType.MIJIN;
            case NetworkType.MIJIN_TEST: 
                return BlockchainNetworkType.MIJIN_TEST;
            case NetworkType.PRIVATE: 
                return BlockchainNetworkType.PRIVATE;
            case NetworkType.PRIVATE_TEST: 
                return BlockchainNetworkType.PRIVATE_TEST;
        }
    }

    static convertApiNode(){
        let url;
        const lastCharacter = ApiNode.apiNode[ApiNode.apiNode.length-1];
        if (lastCharacter == "/"){
            url = ApiNode.apiNode.slice(0,ApiNode.apiNode.length-1);
        }
        else url = ApiNode.apiNode;

        const httpsIs = url.indexOf('https');
        let apiProtocol;
        let apiPort;
        let apiDomain;
        if (httpsIs == -1){
            apiProtocol = Protocol.HTTP;
            if (url.indexOf(':') > -1){
                apiDomain = url.slice(7,url.indexOf(':'));
                apiPort = Number(url.slice(url.indexOf(':')+1));
            }
            else {
                apiDomain = url.slice(7);
                apiPort = 80;
            }
        }
        else {
            apiProtocol = Protocol.HTTPS;
            apiPort = 443;
            apiDomain = url.slice(8);
        }
        // console.log("=====================");
        // console.log(apiProtocol + " " + apiDomain + " " + apiPort);
        // console.log("=====================");
        return {
            'apiDomain' : apiDomain,
            'apiPort' : apiPort,
            'apiProtocol' : apiProtocol
        }
    }

    async getCredential(credentialHash:string ,keyDecrypt:string, senderPublicKey:string){
        const publicAccount = PublicAccount.createFromPublicKey(senderPublicKey,ApiNode.networkType);
        const ipfsConnection = new IpfsConnection(
            CredentialStored.ipfsDomain, // the host or multi address
            CredentialStored.ipfsPort, // the port number
            { protocol: CredentialStored.ipfsProtocol } // the optional protocol
        );
    
        // Creates Proximax blockchain network connection
        const apiHost = CredentialStored.convertApiNode();
        const blockchainConnection = new BlockchainNetworkConnection(
            CredentialStored.convertNetworkType(), // the network type
            apiHost.apiDomain, // the rest api base endpoint
            apiHost.apiPort, // the optional websocket end point
            apiHost.apiProtocol
        );
    
        // Connection Config
        const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);
    
        //
        const dowloader = new Downloader(conectionConfig);

        const downloadParamBuilder = DownloadParameter.create(credentialHash);
        const downloadParam = downloadParamBuilder.build();
        return new Promise(resolve => {
            dowloader.download(downloadParam).then(async result => {
                const content = await result.data.getContentsAsString();
                const encMess = EncryptedMessage.createFromPayload(content);
                const decMess = EncryptedMessage.decrypt(encMess,keyDecrypt,publicAccount);
                // console.log("credential content: " + decMess);
                resolve(JSON.parse(decMess.payload));
            })
        })
    }
}