import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import { Credentials } from './Credentials';
export class CredentialResponseMessage extends AppMessage {

    /**
     * Creation Date of message
     */
    createTimestamp: number;

    private token:string;

    /**
     * Create login request message
     * @param publicKey publicKey of dApp
     * 
     * @param credentials array id of credentials dApp need
     */
    public static create(publicKey: string,token:string,credential:Credentials) {
        return new CredentialResponseMessage(publicKey, token, credential);
    }

    constructor(
        appPublicKey: string,
        sessionToken:string,
        credential:Credentials
    ){
        const type = MessageType.CREDENTIAL_RES;
        const payload = {
            appPublicKey,
            sessionToken,
            credential
        }
        super(type, payload);

        this.createTimestamp = Date.now();
        this.token = payload.sessionToken;

    }

    getSessionToken(){
        return this.token;
    }
}