import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import { CredentialRequired } from './CredentialRequired';

export class VerifyRequestMessage extends AppMessage {

    /**
     * Creation Date of message
     */
    createTimestamp: number;

    private token:string;

    url: string | null | undefined;
    /**
     * Create verify request message
     * @param publicKey publicKey of dApp
     * 
     * @param credentials array id of credentials dApp need
     */
    public static create(
        publicKey: string,
        credentials:CredentialRequired[],
        mode:MessageType, 
        url ? : string | null
    ) {
        const token = this.generateToken();
        return new VerifyRequestMessage(publicKey, token, credentials,mode,url);
    }

    constructor(
        /**
         * dApp 
         */
        appPublicKey: string,
        /**
         * token
         */
        sessionToken: string,
        /**
         * list id
         */
        credentials:CredentialRequired[],
        /**
         * VERIFY_HARD or VERIFY_LIGHT or VERIFY_BOTH
         */
        mode: MessageType,
        /**
         * Use for VERIFY_LIGHT or VERIFY_BOTH
         */
        url ? : string | null
    ) {
        const type = mode;
        const payload = {
            appPublicKey,
            sessionToken,
            credentials,
            url
        }
        super(type, payload);

        this.createTimestamp = Date.now();
        this.token = payload.sessionToken;
        this.url = url;
    }

    /**
     * Generate random string of sesion token
     */
    private static generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 64; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }

    getSessionToken(){
        return this.token;
    }

    getUrl(){
        return this.url;
    }
}