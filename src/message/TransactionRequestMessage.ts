import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';

export class TransactionRequestMessage extends AppMessage {

    constructor(message:string, transaction:any, transactionHash:string){
        let payload = {
            message,
            transaction,
            transactionHash
        }

        let type = MessageType.TRANSACTION;
        
        super(type, payload);
    }

}