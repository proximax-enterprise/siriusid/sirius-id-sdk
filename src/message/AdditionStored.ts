import { Account, NetworkType, EncryptedMessage, PublicAccount } from "tsjs-xpx-chain-sdk";
import { IpfsConnection, BlockchainNetworkConnection, Uploader, Downloader, ConnectionConfig, BlockchainNetworkType, Protocol, StringParameterData, UploadParameter, DownloadParameter, Uint8ArrayParameterData } from 'tsjs-chain-xipfs-sdk';
import {ApiNode} from '../service/ApiNode';
export class AdditionStored{

    static readonly NETWORK = NetworkType.MAIN_NET;
    static readonly ipfsDomain = "ipfs1-dev.xpxsirius.io";

    static readonly ipfsPort = 5443;
    static readonly ipfsProtocol = "https";


    constructor(
        public publicKey:string,
        public privateKey: string,
        public additionHash: any,
    ){
    }

    static create(
        publicKey: string,
        privateKey:string,
        additionHash:string,
    ){  
        return new AdditionStored(publicKey,privateKey,additionHash);
    }

    static convertNetworkType(){
        switch (ApiNode.networkType){
            case NetworkType.MAIN_NET: 
                return BlockchainNetworkType.MAIN_NET;
            case NetworkType.TEST_NET: 
                return BlockchainNetworkType.TEST_NET;
            case NetworkType.MIJIN: 
                return BlockchainNetworkType.MIJIN;
            case NetworkType.MIJIN_TEST: 
                return BlockchainNetworkType.MIJIN_TEST;
            case NetworkType.PRIVATE: 
                return BlockchainNetworkType.PRIVATE;
            case NetworkType.PRIVATE_TEST: 
                return BlockchainNetworkType.PRIVATE_TEST;
        }
    }

    static convertApiNode(){
        let url;
        const lastCharacter = ApiNode.apiNode[ApiNode.apiNode.length-1];
        if (lastCharacter == "/"){
            url = ApiNode.apiNode.slice(0,ApiNode.apiNode.length-1);
        }
        else url = ApiNode.apiNode;

        const httpsIs = url.indexOf('https');
        let apiProtocol;
        let apiPort;
        let apiDomain;
        if (httpsIs == -1){
            apiProtocol = Protocol.HTTP;
            if (url.indexOf(':') > -1){
                apiDomain = url.slice(7,url.indexOf(':'));
                apiPort = Number(url.slice(url.indexOf(':')+1));
            }
            else {
                apiDomain = url.slice(7);
                apiPort = 80;
            }
        }
        else {
            apiProtocol = Protocol.HTTPS;
            apiPort = 443;
            apiDomain = url.slice(8);
        }
        // console.log("=====================");
        // console.log(apiProtocol + " " + apiDomain + " " + apiPort);
        // console.log("=====================");
        return {
            'apiDomain' : apiDomain,
            'apiPort' : apiPort,
            'apiProtocol' : apiProtocol
        }
    }

    async getAddition(){
        // const publicAccount = PublicAccount.createFromPublicKey(senderPublicKey,ApiNode.networkType);
        const ipfsConnection = new IpfsConnection(
            AdditionStored.ipfsDomain, // the host or multi address
            AdditionStored.ipfsPort, // the port number
            { protocol: AdditionStored.ipfsProtocol } // the optional protocol
        );
    
        // Creates Proximax blockchain network connection
        const apiHost = AdditionStored.convertApiNode();
        const blockchainConnection = new BlockchainNetworkConnection(
            AdditionStored.convertNetworkType(), // the network type
            apiHost.apiDomain, // the rest api base endpoint
            apiHost.apiPort, // the optional websocket end point
            apiHost.apiProtocol
        );

        const conectionConfig = ConnectionConfig.createWithLocalIpfsConnection(blockchainConnection, ipfsConnection);
    
        //
        const dowloader = new Downloader(conectionConfig);

        const downloadParamBuilder = DownloadParameter.create(this.additionHash);
        const downloadParam = downloadParamBuilder.withNemKeysPrivacy(this.publicKey,this.privateKey).build();
        return new Promise<any>(resolve => {
            dowloader.download(downloadParam).then(async result => {
                const content = await result.data.getContentAsBuffer();
                resolve(JSON.parse(content.toString()));
            })
        })
    }
}