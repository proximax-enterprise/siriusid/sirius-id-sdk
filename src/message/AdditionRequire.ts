import { AdditionType } from './AdditionType';

export class AdditionRequire{

    private constructor(
        private type:AdditionType,
        private name:string,
        private description:string,
    ){}

    static create(
        type:AdditionType,
        name:string,
        description:string,
    ){
        return new AdditionRequire(type,name,description);
    }

}