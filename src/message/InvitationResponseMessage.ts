import { MessageType } from "./MessageType";
import { QRCode, ErrorCorrectLevel} from 'qrcode-generator-ts/js';
import {SimpleWallet, NetworkType, Password, PublicAccount, Account, BlockHttp, NetworkHttp, BlockInfo, NodeHttp, EncryptedMessage} from 'tsjs-xpx-chain-sdk';
export class InvitationResponseMessage{
    private static globalPrivateKey = "27745A12D8EE237BE979543F76FDDEFE8B266273DDCB4BC23C31DBAF7214F548";
    private static globalPublicKey = "E0665BAB43304CF279F13C7EFB48A857DD03D5BB0372FDC7DAFE9DDAA01ECA5A";

    payload: any;

    constructor(
        messageType: MessageType,
        sessionId: string,
        requestSessionID:string, 
        nodeUrl: string,
        created:any,
        wltBase64: string,
        secretKey: string,
    ){
        this.payload = {
            messageType: messageType,
            sessionId: sessionId,
            requestSessionID: requestSessionID,
            nodeUrl: nodeUrl,
            created:created,
            wltBase64: wltBase64,
            secretKey: secretKey
        };
    }

    static async create(requestSessionID:string, privateKey: string, nodeUrl: string, name:string, password: string, encryptedKey: string, iv: string): Promise<InvitationResponseMessage>{
        let sessionId = this.generateToken();
        let created = Math.floor((new Date).getTime() / 1000);
        let networkHttp = new NetworkHttp(nodeUrl);
        
        return new Promise (resolve => {
            networkHttp.getNetworkType().subscribe(networkType => {
                let account = Account.createFromPrivateKey(privateKey,networkType);
                let wltBase64 = this.base64encode(this.generateWLT(account,name,encryptedKey,iv,networkType))
                resolve(new InvitationResponseMessage(MessageType.INVITATION_RESPONSE,sessionId,requestSessionID,nodeUrl,created,wltBase64,password));
            })
        })
    }

    static base64encode(data:any){
        let buffer = Buffer.from(JSON.stringify(data),'utf8');
        return buffer.toString('base64');
    }

    static base64decode(data:any){
        let buffer = Buffer.from(data, 'base64');
        return JSON.parse(buffer.toString());
    }

    generateQR(){
        let qrCode = new QRCode;
        let data = InvitationResponseMessage.base64encode(this.payload);
        let numberLevel = 17; //data length must < 644
        qrCode.addData(data);
        qrCode.setTypeNumber(numberLevel);
        qrCode.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qrCode.make();
        let url = qrCode.toDataURL();
        return url;
    }

    /**
     * Generate random string of sesion token
     */
    private static generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 7; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }

    static generateWLT(account:Account, name:string, encryptedKey: string, iv: string,networkType: NetworkType){
        let wlt = {
            name: name,
            accounts:[
                {
                    algo:"pass:bip32",
                    address: account.address.plain(),
                    brain: true,
                    default: true,
                    encrypted: encryptedKey,
                    firstAccount: true,
                    iv: iv,
                    name: "Primary_Account",
                    network: networkType,
                    publicAccount: account.publicAccount, 
                    isMultisign: null,
                    nis1Account: null,
                    balance: '0.000000'
                }
            ]
        }
        return wlt;
    }
    
    getNetworkType(urlNode:string): Promise<NetworkType>{
        let networkHttp = new NetworkHttp(urlNode);
        return new Promise (resolve => {
            networkHttp.getNetworkType().subscribe(networkType => {
            resolve(networkType);
            })
        })
    }

    getSessionId(){
        return this.payload['sessionId'];
    }

    getRequestSessionId(){
        return this.payload['requestSessionID'];
    }

    getSecretKey(){
        return this.payload['secretKey'];
    }

    getWltBase64(){
        let base64String = this.payload['wltBase64'];
        let buffer = Buffer.from(base64String, 'base64');
        return JSON.parse(buffer.toString());
    }

    encrypt(publicKey: string){
        let publicAccount = PublicAccount.createFromPublicKey(publicKey,NetworkType.MAIN_NET);
        let encMess = EncryptedMessage.create(JSON.stringify(this.payload),publicAccount,InvitationResponseMessage.globalPrivateKey);
        let buffer = Buffer.from(JSON.stringify(encMess),'utf8');
        return buffer.toString('base64');
    }

    static createFromPayload(message:string, privateKey: string){
        let encMess = InvitationResponseMessage.base64decode(message);
        let pubAccount = PublicAccount.createFromPublicKey(InvitationResponseMessage.globalPublicKey,NetworkType.MAIN_NET);
        let deMess = EncryptedMessage.createFromPayload(encMess.payload);
        let mess =  JSON.parse(EncryptedMessage.decrypt(deMess,privateKey,pubAccount).payload);
        return new InvitationResponseMessage(
            mess['messageType'],
            mess['sessionId'],
            mess['requestSessionID'],
            mess['nodeUrl'],
            mess['created'],
            mess['wltBase64'],
            mess['secretKey']
        );
    }
}