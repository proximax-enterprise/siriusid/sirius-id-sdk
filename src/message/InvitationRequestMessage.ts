import { MessageType } from "./MessageType";
import { QRCode, ErrorCorrectLevel} from 'qrcode-generator-ts/js';
export class InvitationRequestMessage{
    payload: any;
    constructor(
        private description:string, 
        private publicKey: string, 
        private nodeUrl: string
    ){
        let sessionId = this.generateToken();
        let terms = 'https://localhost/terms';
        let created = Math.floor((new Date).getTime() / 1000);
        let expired = created + 15 * 60;
        this.payload = {
            messageType: MessageType.INVITATION_REQUEST,
            sessionId: sessionId,
            description: description,
            publicKeyHex: publicKey,
            nodeUrl: nodeUrl,
            terms: terms,
            created:created,
            expired: expired
        };
        
    }

    static create(description:string, publicKey: string, nodeUrl: string){
        return new InvitationRequestMessage(description,publicKey,nodeUrl);
    }

    base64encode(){
        let buffer = Buffer.from(JSON.stringify(this.payload),'utf8');
        return buffer.toString('base64');
    }

    generateQR(){
        let qrCode = new QRCode;
        let data = this.base64encode();
        let numberLevel = 17; //data length must < 644
        qrCode.addData(data);
        qrCode.setTypeNumber(numberLevel);
        qrCode.setErrorCorrectLevel(ErrorCorrectLevel.L);
        qrCode.make();
        let url = qrCode.toDataURL();
        return url;
    }

    /**
     * Generate random string of sesion token
     */
    private generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 7; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }

    getSessionId(){
        return this.payload['sessionId'];
    }
}