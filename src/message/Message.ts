import { AppId } from './AppId';
import { MessageType } from './MessageType';

export abstract class Message {

    /**
     * Message version
     */
    public readonly version: string = AppId.APP_VERSION;

    constructor(
        /**
         * Message type
         */
        public readonly type: MessageType,
        /**
         * Message payload
         */
        public payload: object
    ) { }

    toString() {
        const message = {
            type: this.type,
            version: this.version,
            payload: this.payload
        }
        return JSON.stringify(message);
    }
}