import { MessageType } from './MessageType';
import { AppMessage } from './AppMessage';
import {TwoFactorAuth} from '../service/TwoFactorAuth';
export class TwoFAMessage extends AppMessage {

    /**
     * Creation Date of message
     */
    createTimestamp: number;

    private token:string;

    /**
     * Create verify request message
     * @param publicKey publicKey of dApp
     * 
     * @param credentials array id of credentials dApp need
     */
    public static create(
        name: string,
        account:string,
    ) {
        const token = this.generateToken();
        TwoFactorAuth.createSecret(name,account);
        return new TwoFAMessage(token, name, account,TwoFactorAuth.secret);
    }

    constructor(
        sessionToken: string,
        /**
         * name
         */
        name:string,
        /**
         * account
         */
        account: string,
        /**
         * account
         */
        secret:any
    ) {
        const type = MessageType.TWO_FA;
        const payload = {
            sessionToken,
            name,
            account,
            secret
        }
        super(type, payload);

        this.createTimestamp = Date.now();
        this.token = payload.sessionToken;
    }

    /**
     * Generate random string of sesion token
     */
    private static generateToken() {
        
        let outString: string = '';
        let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 64; i++) {

            outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

        }
        return outString;
    }

    getSessionToken(){
        return this.token;
    }
}