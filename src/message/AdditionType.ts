export class AdditionType {

    /**
     * image type
     * @type {number}
     */
    public static readonly IMAGE = 1;

    /**
     * document
     * @type {number}
     */
    public static readonly DOCUMENT = 2;
    
    /**
     * document
     * @type {number}
     */
    public static readonly TEXT = 3;
}