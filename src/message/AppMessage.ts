import { Message } from './Message';
import { MessageType } from './MessageType';
import { QRCode, ErrorCorrectLevel, QRNumber, QRAlphaNum, QR8BitByte, QRKanji} from 'qrcode-generator-ts/js';
import {CompressData} from '../service/CompressData';

export abstract class AppMessage extends Message {

    constructor(type: MessageType , payload: object) {
        super(type, payload);
    }

    async generateQR(){
        let data = this.createObject();
        let qrCode = new QRCode;
        //console.log(data.type);
        let dataStr = JSON.stringify(data);
        //console.log(dataStr.length);
        const newData = await CompressData.compress(dataStr);
        //console.log(newData.length);

        let numberLevel = 17;
        if (data.type == MessageType.CREDENTIAL){
            //console.log("Types is Credential");
            if (newData.length < 644){
                numberLevel = 17;
            }
            else if (newData.length < 792){
                numberLevel = 19;
            }
            else if (newData.length < 929){
                numberLevel = 21;
            }
            else if (newData.length < 1091){
                numberLevel = 23;
            }
            else if (newData.length < 1273){
                numberLevel = 25;
            }
            else if (newData.length < 1465){
                numberLevel = 27;
            }
            else if (newData.length < 1732){
                numberLevel = 30;
            }
            else if (newData.length < 2303){
                numberLevel = 35;
            }
            else if (newData.length < 2953){
                numberLevel = 40;
            }
            else{
                console.log('Error:', 'string limit 2953 characters');
                numberLevel = 0;
                return;
            }

            qrCode.addData(newData);
            qrCode.setTypeNumber(numberLevel);
            qrCode.setErrorCorrectLevel(ErrorCorrectLevel.L);
            qrCode.make();
            let url = qrCode.toDataURL();
            return url;
        }
        else{
            if (newData.length > 644){
                console.log('Error:', 'string limit 644 characters');
            }
            else {
                qrCode.addData(newData);
                qrCode.setTypeNumber(17);
                qrCode.setErrorCorrectLevel(ErrorCorrectLevel.L);
                qrCode.make();
                let url = qrCode.toDataURL();
                return url;
            }        
        }
        
    }

    async universalLink(){
        let data = this.createObject();
        let dataStr = JSON.stringify(data);
        const newData = await CompressData.compress(dataStr);

        if (newData.length > 2000){
            console.log('Error:', 'string limit 2000 characters');
        }
        else {
            // let url = "siriusid://siriusid.com/app/data/";
            let url = "otpauth://siriusid/app/data/";
            let replaceStr = newData.replace(new RegExp('/', 'g'),'-');
            return url.concat(replaceStr);
        }
        
    }

    createObject(){
        return {
            'version' : this.version,
            'type' : this.type,
            'payload' : this.payload
        }
    }
}