export class MessageType {

    /**
     * Login message
     * @type {number}
     */
    public static readonly LOG_IN = 1;

    /**
     * Sign transaction request message
     * @type {number}
     */
    public static readonly TRANSACTION = 2;

    /**
     * Send credential message
     * @type {number}
     */
    public static readonly CREDENTIAL = 3;

    /**
     * Credential request message
     * @type {number}
     */
    public static readonly CREDENTIAL_REQ = 4;

    /**
     * Credential request message
     * @type {number}
     */
    public static readonly CREDENTIAL_RES = 5;

    /**
     * Send credendential via Blockchain
     * @type {number}
     */
    public static readonly VERIFY_HARD = 6;

    /**
     * Send credendential via Socket io
     * @type {number}
     */
    public static readonly VERIFY_LIGHT = 7;

    /**
     * Send credendential via Blockchain and Socket io
     * @type {number}
     */
    public static readonly VERIFY_BOTH = 8;

    /**
     * Send credendential via Blockchain and Socket io
     * @type {number}
     */
    public static readonly TWO_FA = 9;

    /**
     * Message which web wallet create for log in by SiriusID 
     * @type {number}
     */
    public static readonly INVITATION_REQUEST = 10;
    
    /**
     * Message which web wallet create for log in by SiriusID 
     * @type {number}
     */
    public static readonly INVITATION_RESPONSE = 11;
}